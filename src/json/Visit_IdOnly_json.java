package json;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonFormat;
/**
 * Basic JSON representation of a Visit-entity only considering the Foreign Key
 * values of the objects relationships. This class can be used to deserialize
 * JSON request-bodies for creating or querying Visits for example.
 * @author timlanghans
 *
 */
@XmlRootElement
public class Visit_IdOnly_json {
   
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    @NotNull(message="Visittime has to be set!")
    private Date visittime;
	
    @NotNull(message="Student_id has to be set!")
    private Integer student_id;
    
    @NotNull(message="liaplace_id has to be set!")
    private Integer liaplace_id;
    
    @NotNull(message="teacher_id has to be set!")
    private Integer teacher_id;


    public Date getVisittime() {
        return visittime;
    }
    public void setVisittime(Date visittime) {
        this.visittime = visittime;
    }
    public Integer getStudent_id() {
        return student_id;
    }
    public void setStudent_id(Integer student_id) {
        this.student_id = student_id;
    }
    public Integer getLiaplace_id() {
        return liaplace_id;
    }
    public void setLiaplace_id(int liaplace_id) {
        this.liaplace_id = liaplace_id;
    }
    public Integer getTeacher_id() {
        return teacher_id;
    }
    public void setTeacher_id(int teacher_id) {
        this.teacher_id = teacher_id;
    }
    
    
    @Override
    public String toString(){
    	return "Visit_json: " + visittime + ", "+student_id + ", " + teacher_id + ", " + liaplace_id;
    }
}