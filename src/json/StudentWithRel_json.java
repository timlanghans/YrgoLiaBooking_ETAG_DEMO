package json;

import java.util.List;

import model.Klass;
import model.Student;

public class StudentWithRel_json {

		private static final long serialVersionUID = 1L;

		private Integer id;
		private String name;
		private List<Klass_json> classes;
		
		public StudentWithRel_json(){}
		
		public StudentWithRel_json(Student s){
			this.id = s.getId();
			this.name = s.getName();
			populateClasses(s);
		}

		private void populateClasses(Student s) {
			for( Klass k : s.getClasses() ){
				this.classes.add(new Klass_json(k));
			}
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}


		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public List<Klass_json> getClasses() {
			return classes;
		}

		public void setClasses(List<Klass_json> classes) {
			this.classes = classes;
		}
}
