package json;

import java.util.Set;

import model.Klass;
import model.Teacher;

public class TeacherWithRel_json {
	private Integer id;
	private String name;
	private Set<Klass_json> klasses;
	
	public TeacherWithRel_json(){}
	
	public TeacherWithRel_json(Teacher t){
		this.id = t.getId();
		this.name = t.getName();
		populateKlasses(t);
	}

	private void populateKlasses(Teacher t) {
		for (Klass k : t.getKlasses()){
			this.klasses.add(new Klass_json(k));
		}
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Set<Klass_json> getKlasses() {
		return klasses;
	}
	
	public void setKlasses(Set<Klass_json> klasses) {
		this.klasses = klasses;
	}
}
