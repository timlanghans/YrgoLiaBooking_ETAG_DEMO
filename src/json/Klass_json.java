package json;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import model.Klass;

@XmlRootElement
public class Klass_json implements Serializable{
	
		private static final long serialVersionUID = 1L;

		private Integer id;
		private String name;

		public Klass_json() {
		}
		
		public Klass_json(Klass klass){
			this.id = klass.getId();
			this.name = klass.getName();
		}
		
		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Integer getId() {
			return this.id;
		}

		public void setId(Integer id) {
			this.id = id;
		}
}
