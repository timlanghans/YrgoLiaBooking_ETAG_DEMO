package json;

import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;

import model.Company;
import model.Liaplace;

@XmlRootElement
public class CompanyWithRel_json {

	private Integer id;
	private String name;
	private Set<Liaplace_json> liaplaces;

	public CompanyWithRel_json() {
	}

	public CompanyWithRel_json(Company c) {
		this.id = c.getId();
		this.name = c.getName();
		populateLiaplaces(c);
	}

	private void populateLiaplaces(Company c) {
		for(Liaplace l : c.getLiaplaces()){
			this.liaplaces.add(new Liaplace_json(l));
		}
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public Set<Liaplace_json> getLiaplaces() {
		return liaplaces;
	}

	public void setLiaplaces(Set<Liaplace_json> liaplaces) {
		this.liaplaces = liaplaces;
	}
	
}
