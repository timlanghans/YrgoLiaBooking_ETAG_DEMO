package json;

import java.util.LinkedHashSet;
import java.util.Set;
import javax.xml.bind.annotation.XmlRootElement;
import model.Klass;
import model.Liaperiod;
import model.Student;
import model.Teacher;

@XmlRootElement
public class KlassWithRel_json {

	private Integer id;	
	private String name;
	private Set<Teacher_json> teachers = new LinkedHashSet<>();
	private Set<Student_json> students = new LinkedHashSet<>();
	private Set<Liaperiod_json> liaPeriods = new LinkedHashSet<>();

	public KlassWithRel_json() {
	}
	
	public KlassWithRel_json(Klass klass){
		this.id = klass.getId();
		this.name = klass.getName();
		populateTeachers(klass);
		populateStudents(klass);
		populateLiaPeriods(klass);
		
	}

	private void populateLiaPeriods(Klass klass) {
		for (Liaperiod l : klass.getLiaPeriods() ){
			this.liaPeriods.add(new Liaperiod_json(l));
		}
	}

	private void populateStudents(Klass klass) {
		for (Student s : klass.getStudents() ){
			this.students.add(new Student_json(s));
		}
		
	}

	private void populateTeachers(Klass klass) {
		for (Teacher t : klass.getTeachers() ){
			this.teachers.add(new Teacher_json(t));
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Teacher_json> getTeachers() {
		return teachers;
	}

	public void setTeachers(Set<Teacher_json> teachers) {
		this.teachers = teachers;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Set<Student_json> getStudents() {
		return students;
	}

	public void setStudents(Set<Student_json> students) {
		this.students = students;
	}

	public Set<Liaperiod_json> getLiaPeriods() {
		return liaPeriods;
	}

	public void setLiaPeriods(Set<Liaperiod_json> liaPeriods) {
		this.liaPeriods = liaPeriods;
	}
}
