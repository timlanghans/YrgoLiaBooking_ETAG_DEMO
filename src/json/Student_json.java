package json;

import java.io.Serializable;

import model.Student;

public class Student_json  implements Serializable{
	private Integer id;
	private String name;
	
	public Student_json(){}
	
	public Student_json(Student s){
		this.id = s.getId();
		this.name = s.getName();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
