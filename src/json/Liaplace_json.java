package json;


import java.io.Serializable;

import model.Liaplace;

public class Liaplace_json implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1231231231L;
	private Integer id;
	private String name;
	
	public Liaplace_json(){}
	
	public Liaplace_json(Liaplace l){
		this.id = l.getId();
		this.name = l.getName();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
