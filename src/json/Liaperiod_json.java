package json;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import model.Liaperiod;

@XmlRootElement
public class Liaperiod_json {

		private Integer id;
		private String name;
		private Date startdate;
		private Date enddate;
		
		public Liaperiod_json() {
		}
		
		public Liaperiod_json(Liaperiod l) {
			this.id = l.getId();
			this.name = l.getName();
			this.startdate = l.getStartdate();
			this.enddate = l.getEnddate();			
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Date getStartdate() {
			return startdate;
		}

		public void setStartdate(Date startdate) {
			this.startdate = startdate;
		}

		public Date getEnddate() {
			return enddate;
		}

		public void setEnddate(Date enddate) {
			this.enddate = enddate;
		}	
		
		
}
