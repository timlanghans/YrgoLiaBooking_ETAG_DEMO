package model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;


/**
 * The persistent class for the teacher database table.
 * 
 */
@Entity
@Table(name="teacher")
@NamedQuery(name="Teacher.findAll", query="SELECT t FROM Teacher t")
public class Teacher implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private Integer id;

	@Column(nullable=false, length=35)
	@Size(min=2)
	private String name;

	//bi-directional many-to-many association to Klass
	@ManyToMany(fetch=FetchType.EAGER, mappedBy="teachers")
	private Set<Klass> klasses;
	
	@OneToMany(mappedBy="teacher", fetch=FetchType.EAGER)
	private Set<Visit> visits;

	public Teacher() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Klass> getKlasses() {
		return this.klasses;
	}

	public void setKlasses(Set<Klass> klasses) {
		this.klasses = klasses;
	}

}