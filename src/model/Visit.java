package model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

/**
 * The persistent class for the visit database table.
 * 
 */
@Entity
@Table(name="visit", uniqueConstraints={@UniqueConstraint(columnNames={"teacher_fk",
		"student_fk","liaplace_fk","visittime"})
})
@NamedQueries({
		@NamedQuery(name= Visit.FIND_ALL,
				query="SELECT v FROM Visit v"),
		@NamedQuery(name= Visit.FIND_BY_STUDENT_ID,
				query="SELECT v FROM Visit v INNER JOIN v.student s WHERE s.id = ?1"),
		@NamedQuery(name= Visit.FIND_BY_TEACHER_ID,
		query="SELECT v FROM Visit v INNER JOIN v.teacher s WHERE s.id = ?1"),
		@NamedQuery(name= Visit.FIND_BY_PLACE_ID,
		query="SELECT v FROM Visit v INNER JOIN v.liaplace s WHERE s.id = ?1")
	})
public class Visit implements Serializable {
	
	private static final long serialVersionUID = 1L;
	public static final String FIND_ALL = "Visit.findAll";
	public static final String FIND_BY_STUDENT_ID = "Visit.findByStudentId";
	public static final String FIND_BY_TEACHER_ID = "Visit.findByTeacherId";
	public static final String FIND_BY_PLACE_ID = "Visit.findByLiaplaceId";
	public static final String FIND_ONE_BY_PK = "Visit.findByVisitPK";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer id;

	@ManyToOne
	@JoinColumn(name="student_fk", nullable=false)
	private Student student;
	
	
	@ManyToOne
	@JoinColumn(name="liaplace_fk", nullable=false)
	private Liaplace liaplace;

	@ManyToOne
	@JoinColumn(name="teacher_fk", nullable=false)
	private Teacher teacher;
	
	@NotNull
	private Date visittime;
	
	
	public Visit() {
	}
	
	public Visit(@NotNull Student stud, @NotNull Liaplace place ,
			@NotNull Teacher teacher, @NotNull Date visittime){
		this.student = stud;
		this.liaplace = place;
		this.teacher = teacher;
		this.visittime = visittime;
	}
	

	public Student getStudent() {
		return student;
	}


	public void setStudent(Student student) {
		this.student = student;
	}


	public Liaplace getLiaplace() {
		return liaplace;
	}


	public void setLiaplace(Liaplace liaplace) {
		this.liaplace = liaplace;
	}


	public Teacher getTeacher() {
		return teacher;
	}


	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public Date getVisittime() {
		return visittime;
	}


	public void setVisittime(Date visittime) {
		this.visittime = visittime;
	}
	
	@Override
	public String toString(){
		return "VISIT id: " + this.id + ", time: " + visittime + "TSL " + teacher.getId() + student.getId() + liaplace.getId();
	}

}