package model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the liaplace database table.
 * 
 */
@Entity
@Table(name="liaplace")
@NamedQuery(name="Liaplace.findAll", query="SELECT l FROM Liaplace l")
public class Liaplace implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private Integer id;

	@Column(nullable=false, length=35)
	private String name;

	//bi-directional many-to-one association to Company
	@ManyToOne
	@JoinColumn(name="company_fk")
	private Company company;
	
	@OneToMany(mappedBy="liaplace", fetch=FetchType.EAGER)
	private Set<Visit> visits;
	
	public Liaplace() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
}