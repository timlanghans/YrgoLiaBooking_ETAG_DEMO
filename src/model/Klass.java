package model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the klass database table.
 * 
 */
@Entity
@Table(name = "Klass")
@NamedQueries({ 
	@NamedQuery(name = Klass.FIND_ALL , query = "SELECT k FROM Klass k"),
	@NamedQuery(name = Klass.FIND_ONE_BY_ID, query = "SELECT k FROM Klass k WHERE k" +
			".id = ?1"),
	@NamedQuery(name = "Klass.findByTeacherId", query = "SELECT k from Klass k INNER JOIN k.teachers ts WHERE ts.id = ?1") })
public class Klass implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String FIND_ALL = "Klass.findAll";
	public static final String FIND_ONE_BY_ID = "Klass.findById";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id", unique = true, nullable = false)
	private Integer id;

	@Column(nullable = false, length = 35)
	private String name;

	// bi-directional many-to-many association to Teacher via join-table
	// "klass_teacher"
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "klass_teacher", joinColumns = @JoinColumn(name = "klass_fk") , inverseJoinColumns = @JoinColumn(name = "teacher_fk") )
	private Set<Teacher> teachers;

	
	@ManyToMany(fetch = FetchType.EAGER, mappedBy="classes")
	private Set<Student> students;
	
	@OneToMany(mappedBy="klass", fetch=FetchType.EAGER)
	private Set<Liaperiod> liaPeriods;
	
	
	public Klass() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Teacher> getTeachers() {
		return this.teachers;
	}

	public void setTeachers(Set<Teacher> teachers) {
		this.teachers = teachers;
	}

	public Set<Student> getStudents() {
		return students;
	}

	public void setStudents(Set<Student> students) {
		this.students = students;
	}

	public Set<Liaperiod> getLiaPeriods() {
		return liaPeriods;
	}

	public void setLiaPeriods(Set<Liaperiod> liaPeriods) {
		this.liaPeriods = liaPeriods;
	}

}