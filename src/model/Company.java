package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the company database table.
 * 
 */
@Entity
@Table(name="company")
// @JsonIdentityInfo(property = "id", generator = ObjectIdGenerators.PropertyGenerator.class)
@NamedQuery(name="Company.findAll", query="SELECT c FROM Company c")
public class Company implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private Integer id;

	@Column(nullable=false, length=35)
	private String name;

	//bi-directional many-to-one association to Liaplace
	@OneToMany(mappedBy="company", fetch=FetchType.EAGER)
	private Set<Liaplace> liaplaces;

	public Company() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Liaplace> getLiaplaces() {
		return this.liaplaces;
	}

	public void setLiaplaces(Set<Liaplace> liaplaces) {
		this.liaplaces = liaplaces;
	}

	public Liaplace addLiaplace(Liaplace liaplace) {
		getLiaplaces().add(liaplace);
		liaplace.setCompany(this);

		return liaplace;
	}

	public Liaplace removeLiaplace(Liaplace liaplace) {
		getLiaplaces().remove(liaplace);
		liaplace.setCompany(null);

		return liaplace;
	}

}