package db;

public class ClientDBException extends DBException{

	public ClientDBException(){
		super();
	}
	
	public ClientDBException(String message){
		super(message);
	}
	
	public ClientDBException(String message, Throwable cause){
		super(message, cause);
	}
	
	
}
