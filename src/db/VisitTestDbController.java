package db;

import javax.ejb.Stateful;
import javax.swing.SwingWorker;
import javax.transaction.Transactional;

import json.Visit_IdOnly_json;
import model.Liaplace;
import model.Student;
import model.Teacher;
import model.Visit;

@Stateful
public class VisitTestDbController extends DBController<Visit,Visit_IdOnly_json>{

	@Override
	public Visit fillEntityFromDTO(Visit entity, Visit_IdOnly_json updateDTO) throws Exception {
		
		entity.setStudent(em.find(Student.class, updateDTO.getStudent_id()));
		entity.setTeacher(em.find(Teacher.class, updateDTO.getTeacher_id()));
		entity.setLiaplace(em.find(Liaplace.class, updateDTO.getLiaplace_id()));
		entity.setVisittime(updateDTO.getVisittime());
		
		if (entity.getStudent() == null || entity.getTeacher() == null || 
				entity.getVisittime() == null || entity.getLiaplace() == null)
			throw new DBException("Cannot create entity from input");
		
		return entity;
	}


	
}
