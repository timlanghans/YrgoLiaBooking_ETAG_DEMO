package db;

public enum DBError {

	ENTITY_NOT_FOUND("Entity_not_found");  
	
	String message;
	
	private DBError(String message){
		this.message = message;
	}
	
	public String getMessage(){
		return message;
	}
	
	
}
