package db;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;


/** 
 * Abstract super class handling all database work via the 
 * {@link javax.persistence.EntityManager}. Can be used for all
 * entities from the {@link model }-package. Subclasses have to implement
 *  <code>fillEntityFromDTO</code> providing the logic for transfering
 *  a DTO-object into a proper Entity-object. This class wraps Exceptions to
 *  the {@link db.DBException} type.  
 * 
 * @author Tim Langhans
 *
 * @param <T> Entity to work with
 * @param <S> The entities DTO object for creation 
 */


@Stateful
public abstract class DBController <T,S> {

	protected Class<T> typee;

	@PersistenceContext(type=PersistenceContextType.EXTENDED)
	protected EntityManager em;

	private static final Logger logger = Logger.getLogger(DBController.class.getName());
	
	public T findOneById(Class<T> entityClass,Integer id) throws Exception {
		try {
			T entity = em.find(entityClass, id);

			if (entity == null)
				throw new DBException();
			return entity;
		} catch (Exception e) {
			throw logAndReturnDBException(e);
		}
	}

	public List<T> findResultListOfNamedQuery(Class<T> entityClass, String queryStr, String... params) throws DBException {
		try {
			TypedQuery<T> query = em.createNamedQuery(queryStr, entityClass);

			for (int i = 1; i <= params.length; i++) {
				query.setParameter(i, params[i]);
			}
			List<T> entities = query.getResultList();
			return entities;
		} catch (Exception e) {
			throw logAndReturnDBException(e);
		}
	}

	public T findOneResultOfNamedQuery(Class<T> entityClass,String queryStr, String... params) throws DBException {
		try {
			TypedQuery<T> query = em.createNamedQuery(queryStr, entityClass);

			for (int i = 1; i <= params.length; i++) {
				query.setParameter(i, params[i]);
			}
			T entity = query.getSingleResult();
			return entity;
		} catch (Exception e) {
			throw logAndReturnDBException(e);
		}

	}

	public void createEntity(Class<T> entityClass , S createDTO) throws DBException{
		try{
			T newEntity = entityClass.newInstance();
			newEntity = fillEntityFromDTO(newEntity, createDTO);
			em.persist(newEntity);
		} catch (Exception e){
			throw logAndReturnDBException(e);
		}
	}
	
	public  void updateEntity(Class<T> entityClass, Integer id , S updateDTO) throws DBException{
		try{
			
			T entity = em.find(entityClass,id);
			
			if (entity == null) throw new DBException("Entity not found", new EntityNotFoundException());
			// virtual method, implemented in concrete sub-class
			entity = fillEntityFromDTO(entity,updateDTO);
			em.persist(entity);
		} catch (Exception e){
			throw logAndReturnDBException(e);
		}
	}
	
	
	public abstract T fillEntityFromDTO (T entity , S updateDTO) throws Exception;
	
	
	public void deleteEntity(Class<T> entityClass, Integer id) throws DBException {
		try {
			T entity = em.find(entityClass, id);
			if (entity == null)
				throw new DBException();
			em.remove(entity);
		} catch (Exception e) {
			throw logAndReturnDBException(e);
		}
	}

	public boolean containsEntity(T entity) throws DBException {
		try {
			return em.contains(entity);
		} catch (IllegalArgumentException e) {
			throw logAndReturnDBException(e);
		}
	}

	// static helper method for logging and throwing of any exception
	private final static DBException logAndReturnDBException(Exception e) {
		logger.log(Level.SEVERE, e.getMessage(), e);
		return new DBException(e.getMessage(), e);
	}

}
