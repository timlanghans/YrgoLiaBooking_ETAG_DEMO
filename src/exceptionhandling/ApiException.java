package exceptionhandling;

import java.util.Locale;

import javax.ws.rs.core.Response;

import db.DBException;
/**
 * Apis custom base exception class. Can wrap different main exceptions from 
 * the applications different tiers, t.ex. <code>DBException</code>. 
 * @author tim langhans
 *
 */
public class ApiException extends RuntimeException {
		 
	    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		
	private Response httpResponse;
	 	 
	    public ApiException(Exception exception) {
	        httpResponse = createHttpResponse(new ApiErrorResponse(exception));
	    }
	 
	    public ApiException(Response.Status status, String messageKey) {
	       httpResponse = createHttpResponse(new ApiErrorResponse(status, messageKey));
	    }
	    
	    public ApiException(DBException e){
	    	httpResponse = createHttpResponse(new ApiErrorResponse(e));
	    }
	    
	    public ApiException(ApiError err){
	    	httpResponse = createHttpResponse(new ApiErrorResponse(err));
	    }

	    public Response getHttpResponse() {
	        return httpResponse;
	    }
	 
	 
	    private static Response createHttpResponse(ApiErrorResponse response) {
	        return Response.status(response.getStatus()).entity(response).build();
	    }
	 
	
}
