package exceptionhandling;

import javax.ws.rs.core.*;
import javax.ws.rs.core.Response.Status;
import javax.xml.bind.annotation.adapters.*;

public class ResponseStatusMarshaller extends XmlAdapter<String, Response.Status>{

	@Override
	public Status unmarshal(String v) throws Exception {
		return Response.Status.valueOf(v);
	}

	@Override
	public String marshal(Status v) throws Exception {
		return v.name();
	}

}
