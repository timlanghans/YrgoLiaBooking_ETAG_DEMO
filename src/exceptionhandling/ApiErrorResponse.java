package exceptionhandling;

import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import db.DBException;

@XmlRootElement
public class ApiErrorResponse {
	public static final String NOT_FOUND = "Not Found";
	public static final String ENTITY_EXISTS = "An entity for the desired values exists already.";
	public static final String UNEXPECTED_ERROR = "An unexpected error has occured. Try again later.";
	public static final String CONSTRAINT_VIOLATION = "User parameters contain values that are not allowed.";
	public static final String JSON_MESSAGE_VALUE = "message";
	public static final String ENTITY_NOT_FOUND = "A required entity was not found. Please check your arguments. ";
	
	@XmlJavaTypeAdapter(value=ResponseStatusMarshaller.class)
	private Response.Status status;
	private String usermessage;
	
	
	public ApiErrorResponse(DBException e){
		this.usermessage = e.getMessage();
		this.status = Response.Status.BAD_REQUEST;
	}
	
	public ApiErrorResponse(Response.Status status , String message){
		this.usermessage = message;
		this.status = status;
	}

	public ApiErrorResponse(ApiError err){
		this.usermessage = err.getMessage();
		this.status = err.getStatus();
	}
	
	
	public ApiErrorResponse(Exception e) {
		this.status = Response.Status.INTERNAL_SERVER_ERROR;
		
		if (e.getMessage() != null){
			this.usermessage = e.getMessage();
		} else {
			this.usermessage = UNEXPECTED_ERROR;
		}
	}
	
	
	public Response.Status getStatus() {
		return status;
	}
	public void setStatus(Response.Status status) {
		this.status = status;
	}
	
	public String getUsermessage() {
		return usermessage;
	}
	public void setUsermessage(String usermessage) {
		this.usermessage = usermessage;
	}
	
	
	
	
	
}
