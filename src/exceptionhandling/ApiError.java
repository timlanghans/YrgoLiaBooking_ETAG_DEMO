package exceptionhandling;

import javax.ws.rs.core.Response;

public enum ApiError {

	ENTITY_NOT_FOUND("Entity not found" , Response.Status.NOT_FOUND),
	ENTITY_ALREADY_EXISTS("Entity already exists" , Response.Status.BAD_REQUEST);
	
	private String message;
	private Response.Status status;
	
	private ApiError(String message, Response.Status status){
		this.message = message;
		this.status = status;
	}
	
	public String getMessage(){
		return message;
	}
	
	public Response.Status getStatus()
	{
		return status;
	}
}
