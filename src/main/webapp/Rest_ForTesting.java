package main.webapp;


import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import json.KlassWithRel_json;
import json.Klass_json;
import json.Teacher_json;
import json.Visit_json;
import model.Company;
import model.Klass;
import model.Student;
import model.Teacher;
import model.Visit;


//@Path("/")
@Transactional
public class Rest_ForTesting {
/*
	
	@Inject
	public EntityManager em;
	
	@GET
	@Path("/teachers")
	@Produces({ MediaType.APPLICATION_JSON})
	public List<Teacher_json> getAllTeachers(){
		TypedQuery<Teacher> query = em.createNamedQuery("Teacher.findAll", Teacher.class);
		List<Teacher> teachers = query.getResultList();
		List<Teacher_json> json = new ArrayList<>();
		
		for (Teacher t : teachers){
			json.add(new Teacher_json(t));
		}
		return json;
	}
	
	@POST
	@Path("/teachers")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addTeacher(Teacher_json body){
		Teacher teacher = new Teacher();
		teacher.setName(body.getName());
		teacher.setEmail(body.getEmail());
		teacher.setTelefon(body.getTelefon());
		
		try{
			em.persist(teacher);
			return Response.ok().build();
		} catch(Exception e){
			e.printStackTrace();
			return Response.serverError().build();
		} finally{
			teacher = null;
		}
	}
	
	
	@GET
	@Path("/students")
	@Produces({ MediaType.APPLICATION_JSON})
	public Set<Student> getAllStudents(){
		TypedQuery<Student> query = em.createNamedQuery("Student.findAll", Student.class);
		return new LinkedHashSet<Student>(query.getResultList());
	}
	
	@GET
	@Path("/classes")
	@Produces({ MediaType.APPLICATION_JSON})
	public List<Klass_json> getAllClazzes(){
		TypedQuery<Klass> query = em.createNamedQuery("Klass.findAll", Klass.class);
		List<Klass> klasses = query.getResultList();
		
		if (klasses.size() == 0) {
			return new ArrayList<>();
		}
		List<Klass_json> json = new ArrayList<Klass_json>(); 
		
		for (Klass k : klasses){
			json.add(new Klass_json(k));
		}
		return json;
	}
	
	@GET
	@Path("/classes/{id}")
	@Produces({ MediaType.APPLICATION_JSON})
	public KlassWithRel_json getAllClazzById(@PathParam("id") Integer id){
		TypedQuery<Klass> query = em.createNamedQuery("Klass.findById", Klass.class);
		query.setParameter(1, id);
		Klass klass;
		try{
			 klass = query.getSingleResult();
		} catch ( PersistenceException e){
			// HTTP 204 No Content
			return null;
		}
		return new KlassWithRel_json(klass); 
	}
	
	@GET
	@Path("/teachers/{id}/classes")
	@Produces({ MediaType.APPLICATION_JSON})
	public List<Klass> getAllClassesForTeacherById(@PathParam("id") Integer id){
		TypedQuery<Klass> query = em.createNamedQuery("Klass.findByTeacherId", Klass.class);
		query.setParameter(1, id);
		return query.getResultList();
	}
	
	@GET
	@Path("/visits")
	@Produces({ MediaType.APPLICATION_JSON})
	public Set<Visit> getAllVisits(){
		TypedQuery<Visit> query = em.createNamedQuery("Visit.findAll", Visit.class);
		return new LinkedHashSet<Visit>(query.getResultList());
	}
	
	
	
	@GET
	@Path("/visits/student/{id}")
	@Produces({ MediaType.APPLICATION_JSON})
	public List<Visit_json> getVisitsByStudentId(@PathParam("id") Integer id){
		TypedQuery<Visit> query = em.createNamedQuery("Visit.findByStudentId", Visit.class);
		query.setParameter(1, id);
		List<Visit> visits = query.getResultList();
		List<Visit_json> json = new ArrayList<>();
		
		for (Visit v : visits){
			json.add(new Visit_json(v));
		}
		return json;
	}
	
	@GET
	@Path("/visits/teacher/{id}")
	@Produces({ MediaType.APPLICATION_JSON})
	public List<Visit_json> getVisitsByTeacherId(@PathParam("id") Integer id){
		TypedQuery<Visit> query = em.createNamedQuery("Visit.findByTeacherId", Visit.class);
		query.setParameter(1, id);
		List<Visit> visits = query.getResultList();
		List<Visit_json> json = new ArrayList<>();
		
		for (Visit v : visits){
			json.add(new Visit_json(v));
		}
		return json;
	}
	
	@GET
	@Path("/visits/liaplace/{id}")
	@Produces({ MediaType.APPLICATION_JSON})
	public List<Visit_json> getVisitsByLiaplaceId(@PathParam("id") Integer id){
		TypedQuery<Visit> query = em.createNamedQuery("Visit.findByLiaplaceId", Visit.class);
		query.setParameter(1, id);
		List<Visit> visits = query.getResultList();
		List<Visit_json> json = new ArrayList<>();
		
		for (Visit v : visits){
			json.add(new Visit_json(v));
		}
		return json;
	}
	
	
	@GET
	@Path("/companies")
	@Produces({ MediaType.APPLICATION_JSON})
	public Set<Company> getAllCompanies(){
		TypedQuery<Company> query = em.createNamedQuery("Company.findAll", Company.class);
		return new LinkedHashSet<Company>(query.getResultList());
	}
	
	
	
	*/
}
