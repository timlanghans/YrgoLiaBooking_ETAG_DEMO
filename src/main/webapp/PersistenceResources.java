package main.webapp;

import javax.ejb.Stateless;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@ApplicationScoped
public class PersistenceResources {

	@Produces
	@PersistenceContext(unitName="webapp")
	private EntityManager em;
}
