package caching;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;

@Provider
public class EtagCreationFilter implements ContainerResponseFilter {

	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
			throws IOException {

		if (!requestContext.getMethod().equals("GET"))
			return;

		String hex_etag = SerializationTools.objectToMD5HexString(responseContext.getEntity());

		String request_etagheader = requestContext.getHeaderString("If-None-Match");
		
		if (request_etagheader != null && request_etagheader.equals(hex_etag)) {
			responseContext.setStatus(304);
			responseContext.setEntity(null);
		} else {
			MultivaluedMap<String, Object> headers = responseContext.getHeaders();
			headers.add("Etag", hex_etag);
		}
	}

}
