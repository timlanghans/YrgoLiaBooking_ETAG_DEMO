package caching;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SerializationTools {

	public static byte[] serialize(Object obj) throws IOException {
		try (ByteArrayOutputStream b = new ByteArrayOutputStream()) {
			try (ObjectOutputStream o = new ObjectOutputStream(b)) {
				o.writeObject(obj);
			}
			return b.toByteArray();
		}
	}

	public static Object deserialize(byte[] bytes) throws IOException, ClassNotFoundException {
		try (ByteArrayInputStream b = new ByteArrayInputStream(bytes)) {
			try (ObjectInputStream o = new ObjectInputStream(b)) {
				return o.readObject();
			}
		}
	}

	public static String md5BytesToEtagHexString(byte[] digest_MD5) {
		final StringBuilder hex = new StringBuilder();

		for (byte b : digest_MD5) {
			hex.append(String.format("%02x", b));
		}
		return hex.toString();
	}
	
	
	public static byte[] digestMD5OfByteArray(byte[] toDigest) throws IOException{
		try {
			MessageDigest digest_MD5 = MessageDigest.getInstance("MD5");
			byte[] digested = digest_MD5.digest(toDigest);
			return digested;
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new IOException("Cannot generate MD5 hash due to missing algorythm");
		}
	}
	
	public static String objectToMD5HexString( Object input) throws IOException{
		final byte[] entity_bytes = SerializationTools.serialize(input);
		final byte[] digested_bytes = SerializationTools.digestMD5OfByteArray(entity_bytes);
		final String hex_etag = SerializationTools.md5BytesToEtagHexString(digested_bytes);
		return hex_etag;
		}
	
	

}
