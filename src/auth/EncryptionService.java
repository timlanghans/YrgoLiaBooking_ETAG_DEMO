package auth;

import java.util.Arrays;

import javax.ejb.Stateless;

import org.mindrot.jbcrypt.BCrypt;

@Stateless
public class EncryptionService {

	private static final int LOG_ROUNDS = 10;

	/**
	 * 
	 * @param password
	 * @return String[] containing the a BCrypt generated salt-string on index 0
	 *         and the resulting password hash on index 1
	 * @return Caution! Returns null on exception!?!?
	 */
	public String[] encrypt(final String password) {

		return this.encrypt(null, password);
	}

	public String[] encrypt(String salt, final String password) {
		if (salt == null) {
			salt = BCrypt.gensalt(LOG_ROUNDS);
		}
		final String hash = BCrypt.hashpw(password, salt);
		final String[] result = { salt, hash };
		System.out.println("SALT AND HASH :" + Arrays.deepToString(result));
		return result;
	}

}
