package auth;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import auth.AccessRole;
import org.hibernate.validator.constraints.Email;




@XmlRootElement 
public class User_json {

	@Id
	@Email(message="Must be a proper email address!") // from org.hibernate validator
	private String email;
	
	@Size(min=7, max=40, message="Password must be between 7 and 40 characters!")
	private String password;

	@Column(name="role")
	@Enumerated(EnumType.STRING)
	private AccessRole userRole;
	
	public User_json(){}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public AccessRole getUserRole() {
		return userRole;
	}
	public void setUserRole(AccessRole userRole) {
		this.userRole = userRole;
	}
	
}
