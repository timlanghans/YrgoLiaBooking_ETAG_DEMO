package auth;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name="users")
public class User {

	@Id
	private String email;

	private String password;

	private String salt;
	
	@Column(name="role")
	@Enumerated(EnumType.STRING)
	private AccessRole userRole;
	
	public User(){}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public AccessRole getUserRole() {
		return userRole;
	}
	public void setUserRole(AccessRole userRole) {
		this.userRole = userRole;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}
	
}
