package auth;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static auth.AccessRole.*;

@Path("/auth")
// @Interceptors({AuthInterceptor.class}) // This works even without adding them
// to Beans.xml
@AccessRoleCheck // OBS! Interceptorbindings must be added to Beans.xml to work!
@Transactional
public class AuthResource {

	private final static Logger logger = Logger.getLogger(AuthResource.class.getName());
	private final String ADMIN = AccessRole.ADMIN.name();
	private final static String TEACHER = AccessRole.TEACHER.name();

	@PersistenceContext
	EntityManager em;

	@Inject
	EncryptionService encryption;

	@RolesAllowed("ADMIN")
	@GET
	@Path("/admin")
	public Response checkAdmin() {
		return Response.ok("The ADMIN has spoken!").build();
	}

	@RolesAllowed("ADMIN")
	@POST
	@Path("/admin/addUser")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addUser(final User_json user) {

		final User newUser = new User();

		try {
			newUser.setUserRole(AccessRole.STUDENT);
			newUser.setEmail(user.getEmail());
			final String[] saltAndPassword = encryption.encrypt(user.getPassword());
			newUser.setSalt(saltAndPassword[0]);
			newUser.setPassword(saltAndPassword[1]);
			em.persist(newUser);
			em.flush();
			return Response.ok().build();
		} catch (ConstraintViolationException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			return Response.status(400).entity("Incorrect parameters: " + e.getMessage()).build();
		} catch (PersistenceException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			return Response.status(400).entity("Email already exists in database!").build();
		} catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			throw new InternalServerErrorException("Something secret went wrong!");
		}
	}

	@RolesAllowed({ "STUDENT", "TEACHER", "ADMIN" })
	@GET
	@Path("/user")
	public Response checkUser() {
		return Response.ok("The USER is online!").build();
	}
}