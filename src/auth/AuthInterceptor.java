package auth;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.core.Response;

import org.mindrot.jbcrypt.BCrypt;



@Interceptor
@AccessRoleCheck
public class AuthInterceptor {

	private static final String AUTHORIZATION_HEADER = "Authorization";
	private static final Response FORBIDDEN = Response.status(403).build();
	private static final Response UNAUTHORIZED = Response.status(401).build();
	private static final Response INTERNAL = Response.status(500).build();
	private final static Logger log = Logger.getLogger(AuthInterceptor.class.getName());

	@Inject
	private HttpServletRequest request;

	@PersistenceContext
	private EntityManager em;

	
	private Logger logger = Logger.getLogger(AuthInterceptor.class.getName());

	@Inject
	EncryptionService enc;

	@AroundInvoke
	public Object checkAuthentication(InvocationContext ctx) throws Exception {

		Method method = ctx.getMethod();

		// Access allowed for all => just return null and go on!
		if (method.isAnnotationPresent(PermitAll.class)) {
			return ctx.proceed();
		}

		// return Access forbidden status 403
		if (method.isAnnotationPresent(DenyAll.class)) {
			throw new ForbiddenException(FORBIDDEN);
		}

		// Method not marked up correctly! Do not allow!
		if (!method.isAnnotationPresent(RolesAllowed.class)) {
			throw new ForbiddenException(FORBIDDEN);
		}

		// Caution, Exceptions handles in method!
		String credentials = fetchCredentialsFromHeader();
		String[] username_password = credentials.split(":");
		final String email = username_password[0];
		final String password = username_password[1];

		// Check credentials with users - database
		// RolesAllowed = a list of SecurityRoles names
		RolesAllowed rolesAnnotation = method.getAnnotation(RolesAllowed.class);
		Set<String> rolesAllowed = new HashSet<String>(Arrays.asList(rolesAnnotation.value()));

		// check if user is authorized
		if (!isUserAllowed(email, password, rolesAllowed)) {
			log.log(Level.WARNING, email + " unauthorized request to endpoint: " + method.getDeclaringClass().getName()
					+ "." + method.getName() + " from " + request.getRemoteAddr() + ":" + request.getRemotePort());
			throw new javax.ws.rs.NotAuthorizedException(UNAUTHORIZED);
		} else {
			// Authorized!
			log.log(Level.INFO, email + " authorized for endpoint: " + method.getDeclaringClass().getName() + "."
					+ method.getName());
			return ctx.proceed();
		}
	}

	// Fetches Credentials from HttpHeader and decodes them
	private String fetchCredentialsFromHeader() {
		String authorization = request.getHeader(AUTHORIZATION_HEADER);

		// No Authorisation information => return 401 Unauthorized
		if (authorization == null || authorization.isEmpty()) {
			throw new javax.ws.rs.NotAuthorizedException(UNAUTHORIZED);
		}

		// Get encoded username and password
		String credentials_enc = authorization.split(" ")[1].trim();

		// Decode credentials to username and password
		String credentials;
		try {
			credentials = new String(java.util.Base64.getDecoder().decode(credentials_enc));
			return credentials;
		} catch (IllegalArgumentException e) {
			throw new javax.ws.rs.InternalServerErrorException(INTERNAL);
		}
	}

	// checks if User has accessrole matching the requested REST-endpoint
	private boolean isUserAllowed(String email, String password, Set<String> rolesAllowed) throws Exception {

		final String query_string = "SELECT u FROM User u WHERE u.email = ?1";

		TypedQuery<User> query = em.createQuery(query_string, User.class);
		query.setParameter(1, email);
		final User user;

		try {
			user = query.getSingleResult();
		} catch (Exception e) {
			return false;
		}

		final String password_check = enc.encrypt(user.getSalt(), password)[1];

		if (password_check.equals(user.getPassword())) {
			return rolesAllowed.contains(user.getUserRole().toString());
		} else {
			log.log(Level.SEVERE, "NOT EQUAL: " + password_check + "  " + user.getPassword() );
			return false;
		}
	}
}
