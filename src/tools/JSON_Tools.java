package tools;

import java.util.ArrayList;
import java.util.List;

import javax.json.Json;

/**
 * Created by timlanghans on 2016-12-04 / 48.
 */
public class JSON_Tools {

	public static final String JSON_MESSAGE_VALUE = "message";
	public static final String CANNOT_CREATE_MESSAGE = "Cannot create DTO " +
			"object.";

	
	/**
	 * Helper method, creating an easy JSON-object wrapping the desired
	 *  error-message to be send to the user. 
	 */
	public static String createJSONerrorMessage(String message){
		return Json.createObjectBuilder()
				.add(JSON_MESSAGE_VALUE, message)
				.build().toString();	
		}	
	
	
	/**
		* Helper method to transform a java.util.List of a Entity-type to a List of
		* the matching DTO-wrapper objects (public JSON representation)
		* @author Tim Langhans
	 * @param entityObjects
	 * @param DTO_wrapper
		*/
	public static final <S,T> List<T> createJsonDtoOfType(
			List<S> entityObjects, Class<T> DTO_wrapper) throws
			IllegalArgumentException{
		
		if (entityObjects == null || DTO_wrapper == null){
			throw new NullPointerException("Arguments may not be null!");
		}
		List<T> json = new ArrayList<>();

		for (S obj : entityObjects) {
				json.add(createNewWrapperObject(obj , DTO_wrapper));
		}
		return json;
	}

	/**
	 * Helper method to transform an Entity-type to
	 * the matching DTO-wrapper object (public JSON representation)
	 * @author Tim Langhans
	 */
	public static final <S,T> T createJsonDtoOfType( S entityObject,
	                                                 Class<T> DTO_wrapper)
			throws IllegalArgumentException {
		if (entityObject == null || DTO_wrapper == null){
			throw new NullPointerException("Arguments may not be null!");
		}
		return createNewWrapperObject(entityObject , DTO_wrapper);
	}

	@SuppressWarnings("unused")
	private static final <S,T> void checkForNullPointer(S entityObject , Class<T> DTO_wrapper){
		if (entityObject == null || DTO_wrapper == null){
			throw new NullPointerException("Arguments may not be null!");
		}
	}
	
	
	

	// helper method for DRY-code
	@SuppressWarnings("unchecked")
	private static final <S,T> T createNewWrapperObject(S entityObject,
	                                                    Class<T> DTO_wrapper) {
		try {
			return (T) Class.forName(DTO_wrapper.getName())
					.getConstructor(entityObject.getClass())
					.newInstance(entityObject);
		} catch (Exception e) {
			throw new IllegalArgumentException(CANNOT_CREATE_MESSAGE);
		}
	}
}
