package tools;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author tim langhans
 *
 */
public class LOG_Tools {

	/** 
	 * Help method to simplify logging of any <code>java.lang.Exception</code>. 
	 * Logs with logging level SEVERE, the exeption message and Throwable.
	 * @param logger {@link java.util.logging.Logger} to log to
	 * @param e - the {@link java.lang.Exception} to log
	 */
		public static void log(final Logger logger, final Exception e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
		}
	
	
}
