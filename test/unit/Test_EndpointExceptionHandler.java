package unit;

import static db.EndpointExceptionHandler.ENTITY_EXISTS;
import static db.EndpointExceptionHandler.JSON_MESSAGE_VALUE;
import static db.EndpointExceptionHandler.NOT_FOUND;
import static db.EndpointExceptionHandler.UNEXPECTED_ERROR;
import static db.EndpointExceptionHandler.handleException;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import javax.persistence.EntityExistsException;
import javax.persistence.NoResultException;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

// TODO Response object from javax.ws needs a Glassfish dependency ?! 
// @RunWith(Parameterized.class)
/*
 * java.lang.RuntimeException: java.lang.ClassNotFoundException: org.glassfish.jersey.internal.RuntimeDelegateImpl
	at javax.ws.rs.ext.RuntimeDelegate.findDelegate(RuntimeDelegate.java:152)
	at javax.ws.rs.ext.RuntimeDelegate.getInstance(RuntimeDelegate.java:120)
	at javax.ws.rs.core.Response$ResponseBuilder.newInstance(Response.java:848)
	at javax.ws.rs.core.Response.status(Response.java:613)
	at rest.EndpointExceptionHandler.handleException(EndpointExceptionHandler.java:32)
	at unit.Test_EndpointExceptionHandler.testOneexception(Test_EndpointExceptionHandler.java:64)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at org.junit.runners.model.FrameworkMethod$1.runReflectiveCall(FrameworkMethod.java:50)
	at org.junit.internal.runners.model.ReflectiveCallable.run(ReflectiveCallable.java:12)
	at org.junit.runners.model.FrameworkMethod.invokeExplosively(FrameworkMethod.java:47)
	at org.junit.internal.runners.statements.InvokeMethod.evaluate(InvokeMethod.java:17)
	at org.junit.internal.runners.statements.RunBefores.evaluate(RunBefores.java:26)
	at org.junit.runners.ParentRunner.runLeaf(ParentRunner.java:325)
	at org.junit.runners.BlockJUnit4ClassRunner.runChild(BlockJUnit4ClassRunner.java:78)
	at org.junit.runners.BlockJUnit4ClassRunner.runChild(BlockJUnit4ClassRunner.java:57)
	at org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)
	at org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)
	at org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)
	at org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)
	at org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)
	at org.junit.runners.ParentRunner.run(ParentRunner.java:363)
	at org.eclipse.jdt.internal.junit4.runner.JUnit4TestReference.run(JUnit4TestReference.java:86)
	at org.eclipse.jdt.internal.junit.runner.TestExecution.run(TestExecution.java:38)
	at org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.runTests(RemoteTestRunner.java:459)
	at org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.runTests(RemoteTestRunner.java:678)
	at org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.run(RemoteTestRunner.java:382)
	at org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.main(RemoteTestRunner.java:192)
Caused by: java.lang.ClassNotFoundException: org.glassfish.jersey.internal.RuntimeDelegateImpl
	at java.net.URLClassLoader.findClass(URLClassLoader.java:381)
	at java.lang.ClassLoader.loadClass(ClassLoader.java:424)
	at sun.misc.Launcher$AppClassLoader.loadClass(Launcher.java:331)
	at java.lang.ClassLoader.loadClass(ClassLoader.java:357)
	at java.lang.Class.forName0(Native Method)
	at java.lang.Class.forName(Class.java:264)
	at javax.ws.rs.ext.FactoryFinder.newInstance(FactoryFinder.java:114)
	at javax.ws.rs.ext.FactoryFinder.find(FactoryFinder.java:207)
	at javax.ws.rs.ext.RuntimeDelegate.findDelegate(RuntimeDelegate.java:135)
	... 29 more


 */
public class Test_EndpointExceptionHandler {

	Exception e;
	int expectedStatusCode;
	String expectedMessage;

	
	//@Parameters
//	public static Iterable<Object[]> data() {
//		return Arrays.asList(new Object[][] { 
//			{ new NoResultException(), 404, NOT_FOUND },
//			{ new EntityExistsException(), 400, ENTITY_EXISTS },
//			{ new IllegalArgumentException(), 500, UNEXPECTED_ERROR }
//			});
//	}

//	public Test_EndpointExceptionHandler(Exception e, int statusCode, String expectedMessage) {
//		this.e = e;
//		this.expectedMessage = expectedMessage;
//		this.expectedStatusCode = statusCode;
//	}

//	@Test
//	public void testAllParameters() {
//		Response response = handleException(e);
//		assertTrue(response.getStatus() == expectedStatusCode);
//		assertTrue(response.getEntity() instanceof String);
//		String jsonobject = (String)response.getEntity();
//		assertTrue(jsonobject.contains(expectedMessage));
//		assertTrue(jsonobject.contains(JSON_MESSAGE_VALUE));
//	}
	
//	@Before
//	public void setup(){
//		this.e = new NoResultException();
//		this.expectedStatusCode = 404;
//		this.expectedMessage = NOT_FOUND;
//	}
//	
//	
//	@Test
//	public void testOneexception(){
//		Response response = handleException(this.e);
//		assertTrue(response.getStatus() == this.expectedStatusCode);
//		assertTrue(response.getEntity() instanceof String);
//		String jsonobject = (String)response.getEntity();
//		assertTrue(jsonobject.contains(this.expectedMessage));
//		assertTrue(jsonobject.contains(JSON_MESSAGE_VALUE));
//	}
}
