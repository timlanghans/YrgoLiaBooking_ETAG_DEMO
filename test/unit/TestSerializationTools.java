package unit;

import org.junit.Assert;
import org.junit.Test;

import caching.SerializationTools;

public class TestSerializationTools {

	
	// 098f6bcd4621d373cade4e832627b4f6
	byte[] bytes_correct = "test".getBytes();
	
	@Test
	public void md5BytesToEtagHexString_shouldReturnCorrectString() throws Exception{
		byte[] encoded_MD5 = SerializationTools.digestMD5OfByteArray(bytes_correct);
		String testString = SerializationTools.md5BytesToEtagHexString(encoded_MD5);
		System.out.println("Returned String: " + testString);
		Assert.assertTrue(testString.equals("098f6bcd4621d373cade4e832627b4f6"));
	}
	
	
	
	
}
