package unit;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import json.KlassWithRel_json;
import json.Klass_json;
import json.Liaperiod_json;
import json.Student_json;
import json.Teacher_json;
import model.Klass;
import model.Liaperiod;
import model.Student;
import model.Teacher;
import tools.JSON_Tools;

public class TestJsonTools {
	
	
	public  Set<Liaperiod> createValidLiaSet(){
		Liaperiod liaperiod = new Liaperiod();
		liaperiod.setName("test_liaperiodname");
		
		liaperiod.setStartdate(new Date(2016 , 10 , 1));
		liaperiod.setEnddate(new Date( 2016, 11, 1 ));
		Set<Liaperiod> periods = new LinkedHashSet<Liaperiod>();
		periods.add(liaperiod);
		return periods;
	}
	
	
	public  Set<Teacher> createValidTeacherSet() {
		Teacher teacher = new Teacher();
		teacher.setId(1);
		teacher.setName("test_teachername");
		Set<Teacher> teachers = new LinkedHashSet<Teacher>();
		teachers.add(teacher);
		return teachers;
	}
	
	
	public Set<Student> createValidStudentSet() {
		Student student = new Student();
		student.setName("test_studentname");
		student.setId(1);
		Set<Student> students = new LinkedHashSet<>();
		students.add(student);
		return students;
	}
	
	
	public  Klass createValidKlassObject(int id){
		Klass klass = new Klass();
		klass.setId(id);
		klass.setName("test_klassname");
		klass.setLiaPeriods(createValidLiaSet());
		klass.setStudents(createValidStudentSet());
		klass.setTeachers(createValidTeacherSet());
		return klass;
	}
	

	@Test
	public void test_createJsonDtoOfType_withCorrectInputKlass_shouldReturnCorrectOutput(){
		Klass klass = createValidKlassObject(1);
		KlassWithRel_json klass_DTO = JSON_Tools.createJsonDtoOfType(klass, KlassWithRel_json.class);
		
		assertTrue(klass_DTO != null);
		assertTrue(klass_DTO.getId() == 1);
		assertTrue(klass_DTO.getName().equals("test_klassname"));
		
		Student_json student = klass_DTO.getStudents().iterator().next();
		assertTrue(student.getName().equals("test_studentname"));
		
		Teacher_json teacher = klass_DTO.getTeachers().iterator().next();
		assertTrue(teacher.getName().equals("test_teachername"));
		
		Liaperiod_json liaperiod = klass_DTO.getLiaPeriods().iterator().next();
		assertTrue(liaperiod.getEnddate().equals(new Date(2016,11,1)));
	}
	
	@Test
	public void test_createJsonDtoOfType_withCorrectInputSetOfKlasses_shouldReturnCorrectOutput(){
		List<Klass> klasses = new ArrayList<>();
		
		for( int i= 0; i < 3 ; i++){
			klasses.add(createValidKlassObject(i));
		}
		List<Klass_json> klass_DTO = JSON_Tools.createJsonDtoOfType(klasses, Klass_json.class);
		
		assertTrue(klass_DTO instanceof List);
		assertTrue(klass_DTO.size() == 3);
		
		// each klass_json should appear once and only once!
		for (Klass_json dto : klass_DTO){
			assertTrue(Collections.frequency(klass_DTO, dto) == 1);
		}
		assertTrue(klass_DTO.get(2).getId() == 2 && 
				klass_DTO.get(2).getName().equals("test_klassname"));
	}
	
	@Test
	public void test_createJsonDtoOfType_withEmptyListOfClasses_shouldReturnAnEmptyList(){
		List<Klass> empty_klasses = new ArrayList<>();
		List<Klass_json> klasses = JSON_Tools.createJsonDtoOfType(empty_klasses, Klass_json.class);
		assertTrue(klasses instanceof List);
		assertTrue(klasses.isEmpty());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void test_createJsonDtoOfType_withEmptyKlassObject_shouldReturnAnEmptyObject(){
		Klass empty_klass = new Klass();
		System.out.println(empty_klass.getId());
		KlassWithRel_json klass_DTO = JSON_Tools.createJsonDtoOfType(empty_klass, KlassWithRel_json.class);
		fail("Should not be able to create a new Object with empty Object!");
	}
	
	
	@Test(expected= IllegalArgumentException.class)
	public void test_createJsonDtoOfType_withIncorrectKlassDTOWrapperClass_shouldThrowIllegalArgumentException(){
		
		JSON_Tools.createJsonDtoOfType(createValidKlassObject(1), Klass.class);
		fail("Should have thrown an IllegalArgumentException!");
	}
	
	@Test(expected= IllegalArgumentException.class)
	public void test_createJsonDtoOfType_withIncorrectTeacherDTOWrapperClass_shouldThrowIllegalArgumentException(){
		
		JSON_Tools.createJsonDtoOfType(createValidKlassObject(1), Teacher_json.class);
		fail("Should have thrown an IllegalArgumentException!");
	}
	
	@Test(expected= NullPointerException.class)
	public void test_createJsonDtoOfType_withNullinArgument1_shouldThrowNullPointerException(){
		
		JSON_Tools.createJsonDtoOfType(null, Klass_json.class);
		fail("Should have thrown a NullPointerException!");
	}
	
	@Test(expected= NullPointerException.class)
	public void test_createJsonDtoOfType_withNullinArgument2_shouldThrowNullPointerException(){
		
		JSON_Tools.createJsonDtoOfType(createValidKlassObject(1), null);
		fail("Should have thrown a NullPointerException!");
	}

}
