package integration;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.as.controller.transform.AddNameFromAddressResourceTransformer;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import json.Klass_json;
import json.Visit_json;
import logger.LoggerProducer;
import main.webapp.PersistenceResources;
import rest.VisitService;
import tools.JSON_Tools;
import tools.LOG_Tools;

@Ignore
@RunAsClient
@RunWith(Arquillian.class)
public class IT_Integrationtest_RestPath_Visit {

		public final static String BASE_PATH = "http://localhost:8080/YRGOLiaBooking/api";
		
		@ArquillianResource
		private  URL baseURL;
		
		@Deployment
		public static WebArchive deploy(){
			WebArchive archive = ShrinkWrap.create(WebArchive.class)
					.addPackage(VisitService.class.getPackage())
					.addPackage(JSON_Tools.class.getPackage())
					.addPackage(LOG_Tools.class.getPackage())
					.addPackage(Klass_json.class.getPackage())
					.addPackage(PersistenceResources.class.getPackage())
					.addPackage(model.Klass.class.getPackage())
					.addPackage(LoggerProducer.class.getPackage())
					.addAsWebInfResource("WEB-INF/beans.xml", "WEB-INF/beans.xml")
					.addAsResource("META-INF/test-persistence.xml" , "META-INF/persistence.xml")
					.addAsResource("META-INF/sql");
					return archive;
		}
	
		@Before
		public  void setupURL() {
//			Persistence.generateSchema("webapp", null); //no persistence provider found! Error!
//			RestAssured.baseURI = BASE_PATH;
			RestAssured.baseURI = this.baseURL.toString();
			RestAssured.basePath = "/api/visits";
			
			System.out.println("I AM LOGGING THE I-TEST: " + RestAssured.baseURI + RestAssured.basePath);
			
		}

		@Before
		public void printIntro(){
			System.out.println("************************************************************");
			
		}
		
		@After
		public void printOutro(){
			System.out.println("______________________________________________________________");
		}
		
		
		@Test
		public void apiIsThere(){
			System.out.println("API for .../visits should return status 200 response");
			System.out.println("GET returns status code: ");
			when().get().then().statusCode(200).log().status();
		}
		
		
		@Test
		public void testVisists_CorrectRequest_shouldReturnCorrectListOfVisists() {
			System.out.printf("Should return a correct list of all visits:\n\n");
			
			given().contentType("application/json")
			.when().get("/")
			.then()
			.body("", hasSize(3))
			.body("[0].student.id" , equalTo(Integer.valueOf(1)) )
			.body("[0].teacher.id" , equalTo(Integer.valueOf(1)) )
			.body("[0].liaplace.id" , equalTo(Integer.valueOf(1)) )
			.statusCode(200)
			.log().all();
		}
		
		
		
		@Test
		public void testVisists_RequestTeacherIdWhichDoesNotExist_shouldReturn200EmptyArray() {
			System.out.printf("RequestteacherIdWhichDoesNotExist_shouldReturn200emptyArray\n\n");
			System.out.printf("Should return a 200  response! \n");
			
			given().contentType("application/json").pathParam("id", 9)
			.when().get("/teacher/{id}")
			.then()
			.statusCode(200)
			.body("", hasSize(0))
			.log().all();
		}
		
		@Test
		public void testvisists_RequestStudentIdWhichDoesNotExist_shouldReturn200EmptyArray() {
			System.out.printf("RequestStudentIdWhichDoesNotExist_shouldReturn200emptyArray\n\n");
			System.out.printf("Should return a 200 response! \n");
			
			given().contentType("application/json").pathParam("id", 9)
			.when().get("/student/{id}")
			.then()
			.statusCode(200)
			.body("", hasSize(0))
			.log().all();
		}
		
		@Test
		public void testVisits_RequestLiaPlaceIdWhichDoesNotExist_shouldReturn200EmptyArray() {
			System.out.printf("RequestLiaplacetIdWhichDoesNotExist_shouldReturn200emptyArray\n\n");
			System.out.printf("Should return a 200 response! \n");
			
			given().contentType("application/json").pathParam("id", 9)
			.when().get("/liaplace/{id}")
			.then()
			.statusCode(200)
			.body("", hasSize(0))
			.log().all();
		}
		
		@Test
		public void testVisists_POSTaNewVisitWithCorrectData_shouldReturn204NoContentAnd400ForSameEntity(){
			System.out.printf("POSTaNewVisitWithCorrectData_shouldReturn204NoContentAnd400ForSameEntity\n\n");
			System.out.printf("Should return a 204 and then a 400 response! \n");
	
			Map<String,String> newVisit = new HashMap<>();
			newVisit.put("visittime","2016-10-10 15:15:15");
			newVisit.put( "student_id", "1");
			newVisit.put("liaplace_id","1");
			newVisit.put("teacher_id", "1");
				  
			given().contentType("application/json").body(newVisit)
			.when()
				.post()
			.then()
				.statusCode(204)
				.log().all();
			
			given().contentType("application/json").body(newVisit)
			.when()
				.post()
			.then()
				.statusCode(400)
				.log().all();
		}
		
		@Test
		// TODO Correct timestamp cannot be found
		public void testVisits_POSTaNewVisitWithCorrectData_shouldPersistCorrectTimeStamp() throws ParseException{
			System.out.printf("POSTaNewVisitWithCorrectData_shouldPersistCorrectTimeStamp\n\n");
			System.out.printf("Insert = '2015-12-30 15:15:15' \n");
			
			// TODO Change when dateformat has been moved to global settings!
			String target = "yyyy-MM-dd HH:mm:ss";
			DateFormat df = new SimpleDateFormat(target);
			Date date =  df.parse("2015-12-30 15:15:15");  
			
			Map<String,String> newVisit = new HashMap<>();
			newVisit.put("visittime", "2015-12-30 15:15:15");
			newVisit.put( "student_id", "2");
			newVisit.put("liaplace_id","2");
			newVisit.put("teacher_id", "2");
				  
			given().contentType("application/json").body(newVisit)
			.when()
				.post()
			.then()
				.statusCode(204)
				.log().all();
			
			Response r = when().get().andReturn();
			System.out.println("GET returned : " + r.getBody().prettyPrint());
			Visit_json[] visit_array = r.as(Visit_json[].class);
			
			System.out.println("All Visits: " + Arrays.deepToString(visit_array));
			Visit_json found = null;
			
			for (Visit_json v : visit_array){
				String dateV = v.getVisittime().toString();
				String dateString = date.toString();
				System.out.println("CHECKING: " + dateV + " = " + date + "?");
				
				if (dateV.equals(dateString) ) {
					found = v;
					System.out.println("MATCH FOUND! ");
				}
			}
			assertTrue("Added visit has to be found in Visits" , found != null);
			assertTrue("Added visit must match all input-data (student)", found.getStudent().getId() == 2);
			assertTrue("Added visit must match all input-data (teacher)", found.getTeacher().getId() == 2);
			assertTrue("Added visit must match all input-data (liaplace)", found.getLiaplace().getId() == 2);
		}
		
		
}
