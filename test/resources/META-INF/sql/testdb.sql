INSERT INTO  Teacher(name) VALUES ('TESTTEACHER1');
INSERT INTO  Klass(name) VALUES ('TESTCLASS1' );
INSERT INTO  Klass_Teacher(Teacher_fk , Klass_fk) VALUES (1,1);
INSERT INTO  Teacher(name ) VALUES ('TESTTEACHER2');
INSERT INTO  Klass_Teacher(Teacher_fk , Klass_fk) VALUES (2,1);
INSERT INTO  Teacher(name) VALUES ('TESTTEACHER3');
INSERT INTO  Klass(name) VALUES ('TESTCLASS2' );
INSERT INTO  Klass_Teacher(Teacher_fk , Klass_fk) VALUES (3,2);
INSERT INTO  LiaPeriod(name,startDate,endDate,klass_fk) VALUES('TESTLIAPERIOD1','2001-01-01', '2001-03-01' , 1 );
INSERT INTO  Student(name) VALUES ('TESTSTUDENT1');
INSERT INTO  Student(name) VALUES ('TESTSTUDENT2');
INSERT INTO  Student(name) VALUES ('TESTSTUDENT3');
INSERT INTO  Enrollment(student_fk,Klass_fk) VALUES (1,2);
INSERT INTO  Enrollment(student_fk,Klass_fk) VALUES (2,1);
INSERT INTO  Enrollment(student_fk,Klass_fk) VALUES (3,1);

INSERT INTO  Company(name) VALUES ('TESTCOMPANY1');
INSERT INTO  Company(name) VALUES ('TESTCOMPANY2');
INSERT INTO  LiaPlace(name, company_fk) VALUES ('TESTLIAPROJECT1' , 1);
INSERT INTO  LiaPlace(name, company_fk) VALUES ('TESTLIAPROJECT2' , 1);
INSERT INTO  LiaPlace(name, company_fk) VALUES ('TESTLIAPROJECT3' , 2);

INSERT INTO  Visit VALUES(1,1,1,'2001-01-01 13:01:01');
INSERT INTO  Visit VALUES(1,2,2,'2002-02-02 14:02:02');
INSERT INTO  Visit VALUES(3,3,3,'2003-03-03 15:03:03');
