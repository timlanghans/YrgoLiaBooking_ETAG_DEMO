CREATE TABLE Teacher(id SERIAL PRIMARY KEY, name varchar(35) NOT NULL, email varchar(50), telefon varchar(15));

CREATE TABLE Klass(  id SERIAL PRIMARY KEY,  name varchar(35) NOT NULL);

CREATE TABLE Klass_Teacher(  Teacher_fk int REFERENCES Teacher(id),  Klass_fk int REFERENCES Klass(id),  PRIMARY KEY( Teacher_fk , Klass_fk));

CREATE TABLE LiaPeriod(  id SERIAL PRIMARY KEY,name VARCHAR(20) NOT NULL,  startDate Date NOT NULL, endDate DATE NOT NULL,  klass_fk int REFERENCES Klass(id));

CREATE TABLE Company( id SERIAL PRIMARY KEY, name varchar(35) NOT NULL, adress varchar(250), telefon varchar(15));

CREATE TABLE LiaPlace(  id SERIAL PRIMARY KEY,  name varchar(35) NOT NULL,  adress varchar(250),  telefon varchar(15),  company_fk int REFERENCES Company(id));

CREATE TABLE Student( id SERIAL PRIMARY KEY,  name varchar(35) NOT NULL,  email varchar(50), telefon varchar(15));

CREATE TABLE Enrollment(  student_fk int REFERENCES Student(id),  Klass_fk int REFERENCES Klass(id),  PRIMARY KEY(student_fk,Klass_fk));

CREATE TABLE Visit(  liaplace_fk int REFERENCES LiaPlace(id),  teacher_fk int REFERENCES Teacher(id),  student_fk int REFERENCES Student(id),  visitTime TIMESTAMP NOT NULL,  PRIMARY KEY(liaplace_fk,teacher_fk,student_fk,visitTime));
