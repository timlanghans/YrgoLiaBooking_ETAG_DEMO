###JUST FOR DEMO!

#How to run: 
This is an excerpt of the YrgoLiaBooking application which is to be run in 
Docker containers to be found in the private Docker-registry of the main project
by the time beeing.

#Why I did this:
Wanted to learn more on caching of web-applications in general and especially 
Etag-header. I created my own Etag header for all GET request payload. Simply 
by MD5 hashing the data I could get from the finished Responsebodies via a 
ContainerResponseFilter. A 304 Not Modified Header is send if the payloads Etag
does not differ from the If-None-Match header received by the client. 